//
//  ViewController.swift
//  TapSpeedTest
//
//  Created by Dallas Johnson on 25/02/2015.
//  Copyright (c) 2015 DallasJohnson. All rights reserved.
//

import UIKit
import QuartzCore

class ViewController: UIViewController {

  var currentTime : CFTimeInterval = 0

  var displayLink : CADisplayLink?
  var frameCount : Int = 0

  let useCAMediaTimeReference : Bool = true

  override func viewDidLoad() {
    super.viewDidLoad()
    displayLink = CADisplayLink(target: self, selector: "displayLinkDidFire:")

    //Initially used this but found it is not accurate and can be affected by external time references.
    //    var time : CFAbsoluteTime = CFAbsoluteTimeGetCurrent()
  }

  override func viewDidAppear(animated: Bool) {
    displayLink?.addToRunLoop(NSRunLoop.mainRunLoop(), forMode: NSRunLoopCommonModes)

  }

  func displayLinkDidFire(link:CADisplayLink){

    //Reset the reference after a few frames so the it's more visually obvious in a sorted line graph where the steps are.

    if frameCount > 3 {
      currentTime = link.timestamp
      frameCount = 0
    } else {
      frameCount++
    }

    //Print out the time difference between CACurrentMediaTime and Displaylink Timestamp
    //        println("DisplayLink_VS_MediaTime \(CACurrentMediaTime() - currentTime)")

  }

  override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
    if let t = touches.first as? UITouch{

      let time = useCAMediaTimeReference ? CACurrentMediaTime() : t.timestamp

      println("tap_ \(time - currentTime) ")

    }
  }

  override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    if let t = touches.first as? UITouch{

      let time = useCAMediaTimeReference ? CACurrentMediaTime() : t.timestamp

      println("lift_ \(time - currentTime)")
      
    }
  }
  
}

